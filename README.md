List of public bugs found by CKI
--------------------------------

How does this work? If a test finds a bug, add a new entry under the name of the
test that found it. This info includes (but is not limited to) external bug
trackers, links to email threads, links to upstream commits after the bug is
resolved and kernel versions.

The main idea of this tracker is to put found bugs into a single place, as they
were distributed across different (mainly internal) places and it was hard to
find a place to link to. Note that the proper way to track new bugs may change
in the future (the initial discussion will happen at Plumbers 2019) and this
repo may end up being automatically generated or abandoned. However for
accountability purposes, let's go ahead with it for now.